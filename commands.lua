COMMANDS = {}

function COMMANDS.hello()
	print( "Hello, world!" )
	print( "OCDOS was started on March 21st, 2014." )
	print( "What a memorable date! :D" )
end

function COMMANDS.cls()
	os.execute( 'cls' )
end

function COMMANDS.lua()
	require( "libs.OCDLib" )
	os.execute( 'cls' )
	os.execute( 'color C' )
	print( "WARNING: The Lua interpreter integration is in a BETA STATE!" )
	print( "You CANNOT return to your OCDOS session when you open the Lua interpreter!" )
	local function choiceprompt()
		io.write( "Continue loading the Lua interpreter (y/n)?: " )
		local choiceConv = io.read()
		return string.lower( choiceConv )
	end

	local choice = choiceprompt()

	if choice == "y" then
		os.execute( "color a" )
		os.execute( 'lua' )
	elseif choice == "n" then
		os.execute( "color a" )
		COMMANDS.cls()
		return
	else
		print( "Invalid input!" )
		OCDLib.pauseAndWait()
		COMMANDS.lua()
	end
end

function COMMANDS.add( ... )
	for k, v in pairs( ... ) do
		_G["num" .. k] = v
		for i = 3, k do
			_G['num' .. i] = nil
		end
	end

	if num1 ~= nil and num2 ~= nil then
		if not tonumber( num1 ) or not tonumber( num2 ) then
			print( "Arguments must be numbers!" )
			return
		end

		local n1 = tonumber( num1 )
		local n2 = tonumber( num2 )

		local result = n1 + n2
		print( num1 .. ' + ' .. num2 .. ' = ' .. result )
		num1 = nil
		num2 = nil
		local n1 = nil
		local n2 = nil
	else
		print( 'Missing arguments!' )
	end
end

function COMMANDS.subtract( ... )
	for k, v in pairs( ... ) do
		_G["num" .. k] = v
		for i = 3, k do
			_G['num' .. i] = nil
		end
	end

	if num1 ~= nil and num2 ~= nil then
		if not tonumber( num1 ) or not tonumber( num2 ) then
			print( "Arguments must be numbers!" )
			return
		end

		local n1 = tonumber( num1 )
		local n2 = tonumber( num2 )

		local result = n1 - n2
		print( num1 .. ' - ' .. num2 .. ' = ' .. result )
		num1 = nil
		num2 = nil
		local n1 = nil
		local n2 = nil
	else
		print( 'Missing arguments!' )
	end
end

function COMMANDS.multiply( ... )
	for k, v in pairs( ... ) do
		_G["num" .. k] = v
		for i = 3, k do
			_G['num' .. i] = nil
		end
	end

	if num1 ~= nil and num2 ~= nil then
		if not tonumber( num1 ) or not tonumber( num2 ) then
			print( "Arguments must be numbers!" )
			return
		end

		local n1 = tonumber( num1 )
		local n2 = tonumber( num2 )

		local result = n1 * n2
		print( num1 .. ' * ' .. num2 .. ' = ' .. result )
		num1 = nil
		num2 = nil
		local n1 = nil
		local n2 = nil
	else
		print( 'Missing arguments!' )
	end
end

function COMMANDS.divide( ... )
	for k, v in pairs( ... ) do
		_G["num" .. k] = v
		for i = 3, k do
			_G['num' .. i] = nil
		end
	end

	if num1 ~= nil and num2 ~= nil then
		if not tonumber( num1 ) or not tonumber( num2 ) then
			print( "Arguments must be numbers!" )
			return
		end

		local n1 = tonumber( num1 )
		local n2 = tonumber( num2 )

		if n2 ~= 0 then
			local result = n1 / n2
			print( num1 .. ' / ' .. num2 .. ' = ' .. result )
		else
			print( "ERROR: Dividing by 0 is not possible!" )
		end
		
		num1 = nil
		num2 = nil
		local n1 = nil
		local n2 = nil
	else
		print( 'Missing arguments!' )
	end
end

function COMMANDS.modulo( ... )
	for k, v in pairs( ... ) do
		_G["num" .. k] = v
		for i = 3, k do
			_G['num' .. i] = nil
		end
	end

	if num1 ~= nil and num2 ~= nil then
		if not tonumber( num1 ) or not tonumber( num2 ) then
			print( "Arguments must be numbers!" )
			return
		end

		local n1 = tonumber( num1 )
		local n2 = tonumber( num2 )

		local result = n1 % n2
		print( num1 .. ' % ' .. num2 .. ' = ' .. result )
		num1 = nil
		num2 = nil
		local n1 = nil
		local n2 = nil
	else
		print( 'Missing arguments!' )
	end
end