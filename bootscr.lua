-- Will probably style later.

function bootscreen( ver ) --Add an argument for easy version number displaying.
	os.execute( 'cls' ) -- Clean up any extraneous stuff.

	-- Should probably make this different.
	print( 'Loading OCDOS version ' .. ver .. '...' )
	print( 'Loaded.' )
	print( [[

=======================================
|                                     |
|         OCDOS version 1.02          |
|      Developed by Short Circuit     |
|                                     |
=======================================
	]] )
end