--[[

	=======================================
	|                                     |
	|         OCDOS version 1.02          |
	|      Developed by Short Circuit     |
	|                                     |
	=======================================

]]--

OCDOS_VERSION = 1.02

require( "requires" )

function init( ver )
	os.execute( "color a" )
	bootscreen( ver )
	interpreter()
end

-- For debugging

--[[
print( "ERROR: Unhandled exception occured in command check!" )
error()
]]--


init( OCDOS_VERSION )