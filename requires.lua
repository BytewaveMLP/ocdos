--Create a list of all required OS files- this makes it easier to upgrade everything.
OCDOS_FILES = {
"interpreter", 
"commands",
"bootscr",
"libs.OCDLib"
}

function LOAD_REQUIRED( f )
	--Load all required files
	for k, v in pairs( f ) do
		require( v )
	end
end

LOAD_REQUIRED( OCDOS_FILES )