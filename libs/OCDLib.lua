-- Private OCDLib version for OCDOS- minimized file size (no commenting)

OCDLib = {}

OCDLibPrivateFuncs = {}

version = "0.7.3"

function OCDLibPrivateFuncs.errorDetection( func )
	error( "[OCDLib]: OCDLib." .. func .. "() experienced an error. Please check your syntax." )
end

function OCDLib.makeSomeRoom( spaces )
	if spaces ~= nil then
		for i = 1, spaces do
			print( "" )
		end
	elseif spaces == nil then
		for i = 1, 2 do
			print( "" )
		end
	else
		OCDLibPrivateFuncs.errorDetection( "makeSomeRoom" )
	end
end

function OCDLib.pauseAndWait( textstr )
	if textstr ~= nil then
		print( textstr )
	elseif textstr == nil then
		print( "Press Enter to continue..." )
	else
		OCDLibPrivateFuncs.errorDetection( "pauseAndWait" )
	end
	io.read()
end

function OCDLib.trueRandom( minpass, maxpass )
	return OCDLib.betterRandom( minpass, maxpass )
end

function OCDLib.betterRandom( min, max, numberToGen )
	math.randomseed( os.time() )
	uselessDumpingGrounds = {}
	for i = 1,10 do
		uselessDumpingGrounds[i] = math.random()
	end
	uselessDumpingGrounds = nil
	if numberToGen == nil then
		if min and max ~= nil then
			random = math.random( min, max )
		elseif min == nil then
			random = math.random( max )
		elseif max == nil then
			random = math.random( min, 10 )
		elseif min and max == nil then
			random = math.random( 10 )
		else
			OCDLibPrivateFuncs.errorDetection( "betterRandom" )
		end
	elseif numberToGen ~= nil then
		random = {}
		for i = 1, numberToGen do
			if min and max ~= nil then
				random[i] = math.random( min, max )
			elseif min == nil then
				random[i] = math.random( max )
			elseif max == nil then
				random[i] = math.random( min, 10 )
			elseif min and max == nil then
				random[i] = math.random( 10 )
			else
				OCDLibPrivateFuncs.errorDetection( "betterRandom" )
			end
		end
	else
		OCDLibPrivateFuncs.errorDetection( "betterRandom" )
	end
	return random
end

function OCDLib.credits( scriptname, creator, date, use )
	if scriptname ~= nil and creator ~= nil and date ~= nil and use ~= nil then
		print( "Script name: " .. scriptname )
		print( "Author: " .. creator )
		print( "Created on: " .. date )
		print( "Used for: " .. use )
	elseif use == nil or date == nil or creator == nil or scriptname == nil then
		error( "OCDLib.credits(): You must specify every argument! *Note- a \"\" will work." )
	else
		OCDLibPrivateFuncs.errorDetection( "credits" )
	end
end

function OCDLib.repeatPrint( printStr, numTimes )
	i = 1
	if numTimes ~= nil then
		for i = 1, numTimes do
			print( printStr )
		end
	elseif numTimes == nil then
		for i = 1, 5 do
			print( printStr )
		end
	else
		OCDLibPrivateFuncs.errorDetection( "repeatPrint" )
	end
end

function OCDLib.dumpTable( tab )
	for k, v in ipairs( tab ) do
		print( k  .. ": " .. v )
	end
end

function OCDLib.version()
end

function OCDLibPrivateFuncs.testLib()
	print( "\nOCDLib.version()" )
	OCDLib.version()
	print( "\nOCDLib.dumpTable()" )
	tableToDump = { "pie", "cupcakes", "cake", "apples" }
	OCDLib.dumpTable( tableToDump )
	print( "\nOCDLib.makeSomeRoom()" )
	OCDLib.makeSomeRoom( 2 )
	print( "\nOCDLib.betterRandom() legacy support test" )
	rand = OCDLib.trueRandom( nil, 45 )
	print( rand )
	print( "\nOCDLib.betterRandom()" )
	rand2 = OCDLib.betterRandom( nil, 45 )
	print( rand2 )
	print( "\nOCDLib.credits()" )
	OCDLib.credits( "OCDLib Test",
		"Short Cirucit",
		"2/22/14",
		"Testing the OCDLib functions as they exist." )
	print( "\nOCDLib.repeatPrint()" )
	OCDLib.repeatPrint( "hai guys", 3 )
	print( "\nOCDLib.isDefined() (defined var)" )
	variable = 10
	print( OCDLib.isDefined( variable ) )
	print( "\nOCDLib.isDefined() (nil var)" )
	print( OCDLib.isDefined( nilVar ) )
	print( "\nOCDLib.pauseAndWait()" )
	OCDLib.pauseAndWait( "Press enter to complete the test..." )
	print( "\nOCDLibPrivateFuncs.errorDetection()" )
	OCDLibPrivateFuncs.errorDetection( "testLib" )
end

function OCDLib.isDefined( var )
	if var ~= nil then
		return true
	else
		return false
	end
end

function OCDLib.abs( number )
	assert( type( number ) == "number", "[OCDLib]: OCDLib.abs() expects a number value!" )
	return number >= 0 and number or -number
end

function OCDLib.tableLength( tab )
	local count = 0
	
	for _ in pairs( tab ) do
		count = count + 1
	end

	return count
end

function OCDLib.explode( d, p )
	local t, ll
	t = {}
	ll = 0
	if (#p == 1) then return {p} end
	while true do
		l = string.find(p,d,ll,true)
		if l ~= nil then
			table.insert( t, string.sub( p, ll, l - 1 ) )
			ll = l + 1
		else
			table.insert( t, string.sub( p, ll ) )
			break
		end
	end
	return t
end