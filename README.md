#OCDOS
##An MS-DOS based command line OS written by an OCD brony

Welcome to **OCDOS**, an operating system coded in Lua by Short Circuit- an OCD brony who is a modestly mediocre at best coder who wishes to test his skills on Lua.
OCDOS is a command line OS built in Lua that can execute basic commands in an MS-DOS style fashion (prompt: command -args-).

###CURRENT VERSION:

**1.02**

###OCDOS CHANGELOG:

**v1.02:** Added command arguments and things.

**v1.01:** Initial Git release.

**v1.0:** File creations and bits of code released.

###TODO:

-	More commands