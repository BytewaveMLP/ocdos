function prompt()
	require( "requires" ) -- apparently must do this upon every prompt.

	io.write( "OCDOS: " )
	
	local inputr = io.read()

	if inputr == "" or inputr == nil then
		print 'No command entered! Try again!'
		interpreter()
	else
		input = OCDLib.explode( " ", inputr )
		--[[
		for k, v in pairs( input ) do
			print( "DEBUG (inputr: " .. k .. "):" .. v )
		end
		]]--
		return input
	end
end

function interpreter()
	OCDOS = {}

	OCDOS.VERSION = "1.02"

	OCDOS.GIT_LINK = "https://github.com/ShortCircuittheBrony/OCDOS"

	require( "requires" )

	-- I will add a fix for stuff like this when I get the chance.
	OCDOS.VALID_COMMANDS = {
	"hello",
	"cls",
	"add",
	"subtract",
	"multiply",
	"divide",
	"modulo",
	"help",
	"ver",
	"lua"
	}

	-- Built in help command because I'm lazy!
	OCDOS.COMMANDS_INFO = {
	"Hello, world!",
	"Clears the screen.",
	"Adds two numbers (supplied by command arguemnts) together.",
	"Subtracts two numbers (suplied by command arguemnts) from each other.",
	"Multiplies two numbers (supplied by command arguments) by each other.",
	"Divides two numbers (supplied by command argumetns) by each other.",
	"Divides two numbers (supplied by command arguments) by each other and returns the remaider of the equation.",
	"Displays a list of commands and what they do.",
	"Tells you more about what you're using.",
	"Opens an interactive Lua prompt (BETA)."
	}

	failedTimes = 0

	print()

	local OCDOS_IN_RAW = prompt()
	local OCDOS_IN = string.lower( OCDOS_IN_RAW[ 1 ] )

	-- Will add a debug mode for these commented out lines to run soon. ;P

	for k, v in pairs( OCDOS.VALID_COMMANDS ) do
		if v == OCDOS_IN then
			OCDOS.EXEC = OCDOS_IN
			-- print( 'DEBUG (OCDOS_IN): ' .. OCDOS_IN )
		else
			failedTimes = failedTimes + 1
			-- print( 'DEBUG (failedTimes): ' .. failedTimes )
		end
	end

	local valid_command_ct = OCDLib.tableLength( OCDOS.VALID_COMMANDS )
	-- print( 'DEBUG (valid_command_ct): ' .. valid_command_ct )
	for i = 1, valid_command_ct do
		-- print( 'DEBUG (i): ' .. i )
		if OCDOS.EXEC == OCDOS.VALID_COMMANDS[ i ] and failedTimes < valid_command_ct then
			local args = {}
			for k, v in pairs( OCDOS_IN_RAW ) do
				if k > 1 then
					num = k - 1
					args[ num ] = OCDOS_IN_RAW[ k ]
				end
			end

			if OCDOS.EXEC == "help" then
				print( "\n=>------------------- OCDOS HELP -------------------<=" )
				for k, v in pairs( OCDOS.VALID_COMMANDS ) do
					if OCDOS.COMMANDS_INFO[ k ] ~= nil or OCDOS.VALID_COMMANDS[ k ] ~= nil then
						print( v .. ": " .. OCDOS.COMMANDS_INFO[ k ] )
					end
				end
				print( "=>--------------------------------------------------<=\n" )
			elseif OCDOS.EXEC == "ver" then
				print( "\n=>------------------- OCDOS INFO -------------------<=" )
				print( "You are running OCDOS version: " .. OCDOS.VERSION )
				print( "To check for a new version, you may view this link:\n" )
				print( OCDOS.GIT_LINK )
				print( "=>--------------------------------------------------<=\n" )
			else
				if args ~= nil then
					_G["COMMANDS"][OCDOS.EXEC]( args )
				else
					_G["COMMANDS"][OCDOS.EXEC]()
				end
			end
			interpreter()
		elseif failedTimes >= valid_command_ct then
			print( 'Bad command entered! Check your spelling and try again!' )
			interpreter()
		else
			if i > valid_command_ct then
				-- Gotta have dem checks for errors tho.
				print( "ERROR: Unhandled exception occured in command check!" )
				error()
			end
		end
	end
end